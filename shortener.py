#!/usr/bin/python3

import webapp
from urllib.parse import unquote

formulario = """<form action="" method="POST">
             <h1>URL a acortar</h1>
             <input type="text" name="URL" value="">
             <input type="submit" value="Enviar">
             </form>"""

class shortener (webapp.webApp):

    url_original = {}
    url_acortada = {}

    def parse(self, request):
        method = request.split(' ', 1)[0]
        resource = request.split(' ', 2)[1]
        body = request.split('=')[-1]
        return method, resource, body

    def process(self, peticion):
        self.urlsContador = len(self.url_original)
        method, resource, url_original = peticion
        if method == "GET":
            if resource == "/":
                http_code = "HTTP/1.1 200 OK"
                html_body = ("<html><body>" + formulario + "<h4>Url guardada: <h4>"  +  str(self.url_original)
                             + "</body></html>")
            elif resource == "/favicon.ico":
                http_code = "HTTP/1.1 404 Not Found"
                html_body = ("<html><body><h1>Not Found</h1></body></html>")
            else:
                resource = int(resource[1:])
                if resource in self.url_acortada:
                    http_code = "HTTP/1.1 302 Found"
                    html_body = ("<html><body><meta http-equiv='refresh'"
                                 + "content='1 url=" + self.url_acortada[resource] +"</body></html>")
                else:
                    http_code = "HTTP/1.1 404 Not Found"
                    html_body = "<html><body><h1>Recurso no disponible</h1></body></html>"
        elif method == "POST":
            if url_original == "":
                http_code = "HTTP/1.1 204"
                html_body = "<html><body><h1>Formulario vacio</h1></body></html>"

            else:
                if (url_original[0:14] == "https%3A%2F%2F" or
                        url_original[0:13] == "http%3A%2F%2F"):
                    url_original = unquote(url_original)
                else:
                    url_original = "http://" + url_original
                if url_original in self.url_original:
                    url = "http://localhost:1234/" + str(self.url_original[url_original])
                    http_code = "HTTP/1.1 200 Ok"
                    html_body = ("<html><body>Url acortada: <a href=" + url + ">" + url + "</a></body></html>")
                else:
                    self.url_original[url_original] = self.urlsContador
                    url = "http://localhost:1234/" + str(self.urlsContador)
                    self.url_acortada[self.urlsContador] = url_original
                    self.urlsContador = self.urlsContador + 1
                    http_code = "HTTP/1.1 200 Ok"
                    html_body = ("<html><body>Url acortada: <a href=" + url + ">" + url + "</a></body></html>")
        else:
            http_code = "HTTP/1.1 405 Method Not Allowed"
            html_body = "<html><body><h1>Método no valido<h1/></body></html>"
        return http_code, html_body

if __name__ == "__main__":
    testWebApp = shortener("localhost", 1234)
